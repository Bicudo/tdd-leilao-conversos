package com.tdd.Leilao.services;

import com.tdd.Leilao.Dtos.LanceDTO;
import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Usuario;
import com.tdd.Leilao.repositories.LanceRepository;
import com.tdd.Leilao.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanceService {
    @Autowired
    private LanceRepository lanceRepository;

    //criando um usuario
    public Lance criarLance (LanceDTO lanceDTO){
        Lance lanceObjeto = lanceDTO.converterParaLance();
        return lanceRepository.save(lanceObjeto);
    }


}
