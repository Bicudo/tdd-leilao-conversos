package com.tdd.Leilao.services;

import com.tdd.Leilao.models.Usuario;
import com.tdd.Leilao.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    //criando um usuario
    public Usuario criarUsuario (Usuario usuario){
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }


    public Iterable<Usuario> lerTodosOsUsuarios () {

        return usuarioRepository.findAll();
    }

    public Usuario buscarUsuarioPeloId (int id){

        Optional<Usuario> usuarioOptional= usuarioRepository.findById(id);

        if(usuarioOptional.isPresent()){
            Usuario usuario = usuarioOptional.get();
            return usuario;
        }else{
            throw new RuntimeException("O usuario nao foi encontrado");
        }
    }

}
