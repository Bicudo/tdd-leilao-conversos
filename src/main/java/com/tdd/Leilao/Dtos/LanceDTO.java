package com.tdd.Leilao.Dtos;

import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Usuario;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

public class LanceDTO {

    private double valor;

    @NotNull
    private Usuario usuario;

    public LanceDTO() {
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Lance converterParaLance(){
        Lance lance = new Lance();
        lance.setValor(this.valor);
        return lance;
    }

}
