package com.tdd.Leilao.repositories;

import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Leilao;
import org.springframework.data.repository.CrudRepository;

public interface LeilaoRespository extends CrudRepository<Leilao,Integer> {
}
