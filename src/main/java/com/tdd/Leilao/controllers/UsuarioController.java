package com.tdd.Leilao.controllers;

import com.tdd.Leilao.models.Usuario;
import com.tdd.Leilao.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario criarUsuario (@RequestBody @Valid Usuario usuario){
        Usuario objetoUsuario = usuarioService.criarUsuario(usuario);
        return objetoUsuario;
    }

    @GetMapping //Vai ler todos os recursos do banco
    public Iterable<Usuario> lerTodosOsUsuarios (){
        return usuarioService.lerTodosOsUsuarios();
    }
}
