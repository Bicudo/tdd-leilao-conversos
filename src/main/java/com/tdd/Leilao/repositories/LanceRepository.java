package com.tdd.Leilao.repositories;

import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface LanceRepository extends CrudRepository<Lance,Integer> {

}
