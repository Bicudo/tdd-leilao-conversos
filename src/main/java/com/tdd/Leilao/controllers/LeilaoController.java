package com.tdd.Leilao.controllers;

import com.tdd.Leilao.Dtos.LanceDTO;
import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Leilao;
import com.tdd.Leilao.services.LanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/leilao")
public class LeilaoController {

    @Autowired
    private LanceService lanceService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lance adicionarLance (@RequestBody LanceDTO LanceDTO){
            Lance lance = lanceService.criarLance(LanceDTO);
            return lance;
    }
}
