package com.tdd.Leilao.controllers;

import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Leiloeiro;
import com.tdd.Leilao.models.Usuario;
import com.tdd.Leilao.services.LeiloeiroService;
import com.tdd.Leilao.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/leiloeiro")
public class LeiloeiroController {

    @Autowired
    LeiloeiroService leiloeiroService;

    @PostMapping("/adicionarLance")
    @ResponseStatus(HttpStatus.CREATED)
    public Leiloeiro criarLeiloeiro (@RequestBody @Valid Leiloeiro leiloeiro){
        Leiloeiro objetoLeiloeiro = leiloeiroService.criarLeiloeiro(leiloeiro);
        return objetoLeiloeiro;
    }

    @GetMapping("/pegaMaiorLance")
    public Lance criarLeiloeiro (@RequestBody @Valid Leiloeiro leiloeiro){
        Leiloeiro objetoLeiloeiro = leiloeiroService.criarLeiloeiro(leiloeiro);
        return objetoLeiloeiro;
    }
}
