package com.tdd.Leilao.repositories;

import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Leiloeiro;
import org.springframework.data.repository.CrudRepository;

public interface LeiloeiroRespository extends CrudRepository<Leiloeiro, Integer> {
}
