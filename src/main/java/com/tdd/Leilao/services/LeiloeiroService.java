package com.tdd.Leilao.services;

import com.tdd.Leilao.models.Lance;
import com.tdd.Leilao.models.Leilao;
import com.tdd.Leilao.models.Leiloeiro;
import com.tdd.Leilao.repositories.LeilaoRespository;
import com.tdd.Leilao.repositories.LeiloeiroRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/leiloeiro")
public class LeiloeiroService {

    @Autowired
    private LeiloeiroRespository leiloeiroRespository;

    //criando um usuario
    public Leiloeiro criarLeiloeiro (Leiloeiro leiloeiro){
        Leiloeiro leiloeiroObjeto = leiloeiroRespository.save(leiloeiro);
        return leiloeiroObjeto;
    }

}
